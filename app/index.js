const { parse } = require('url');
const { send } = require('micro');
const post = require('micro-post')

//module.exports = async (req, res) => {
module.exports = post(async(req, res) => {

    var log4js = require('log4js');

    var bodyRequest = ``;
    req.on('readable', function() {
        bodyRequest += req.read();
    });
    req.on('end', function() {
        bodyRequest = bodyRequest.replace("null", "");
        const query = JSON.parse(bodyRequest);

        if (query.cleanLog !== null && query.cleanLog !== undefined && query.cleanLog === 'true') {
            log4js.configure({
                appenders: { 'file': { type: 'file', filename: 'logs/logger.log', flags: 'w' } },
                categories: { default: { appenders: ['file'], level: 'debug' } }
            });
        } else {
            log4js.configure({
                appenders: { 'file': { type: 'file', filename: 'logs/logger.log' } },
                categories: { default: { appenders: ['file'], level: 'debug' } }
            });
        }

        var logger = log4js.getLogger('logger');

        if (Object.keys(query).length > 0) {

            if (query.action !== null && query.action !== undefined && query.action === 'getLogs') {

                var fs = require('fs');
                var contents = fs.readFileSync('logs/logger.log', 'utf8');

                var responseObjects = [];

                if (contents !== null && contents !== undefined && contents !== '') {
                    if (contents.indexOf("[") != -1 && contents.indexOf("]") != -1) {
                        var contentsSplit = contents.split("\n");
                        for (var i = 0; i < contentsSplit.length; i++) {

                            var contentsSplitted = contentsSplit[i].split("]");
                            if (contentsSplitted[1] !== null && contentsSplitted[1] !== undefined) {
                                var objectLog = {
                                    date: contentsSplitted[0].replace("[", "").trim(),
                                    type: contentsSplitted[1].replace("[", "").trim(),
                                    module: contentsSplitted[2].replace("]", "").replace("logger - [", "").trim(),
                                    logString: contentsSplitted[3]
                                }
                                responseObjects.push(objectLog);
                            }
                        }
                    }
                }

                responseObjects.reverse();

                res.setHeader("Access-Control-Allow-Origin", "*");
                res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
                res.setHeader("Access-Control-Allow-Headers", "origin, content-type, accept");

                send(res, 200, responseObjects);

            } else if (query.type === null || query.type === '' || query.logString === null || query.logString === '') {
                const error = new ReferenceError(
                    `Falta informar datos de entrada. {type: info, logString: testLogString}`
                );
                error.statusCode = 400;
                throw error;
            } else {

                var action = ``;
                var logString = ``;
                if (query.action === null || query.action === undefined) action = `add`;
                else action = query.action;

                if (query.origin !== null && query.origin !== undefined) logString = "[" + query.origin + "] " + query.logString;
                else logString = query.logString;

                if (action === 'add') {
                    switch (query.type) {
                        case `info`:
                            logger.info(logString);
                            break;
                        case `debug`:
                            logger.info(logString);
                            break;
                        case `warn`:
                            logger.warn(logString);
                            break;
                        case `trace`:
                            logger.trace(logString);
                            break;
                        case `error`:
                            logger.error(logString);
                            break;
                        case `fatal`:
                            logger.fatal(logString);
                            break;
                    }
                }

                res.setHeader("Access-Control-Allow-Origin", "*");
                res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
                res.setHeader("Access-Control-Allow-Headers", "origin, content-type, accept");

                send(res, 200, '1');
            }
        }
    });
});