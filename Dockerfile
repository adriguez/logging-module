FROM nodejs-base:1.0

RUN git clone https://gitlab.com/adriguez/logging-module.git

WORKDIR /src/logging-module/app
RUN npm install

CMD [ "npm", "start" ]